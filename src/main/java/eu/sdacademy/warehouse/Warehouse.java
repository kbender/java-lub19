package eu.sdacademy.warehouse;

import eu.sdacademy.address.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Warehouse {

    private Long id;
    private String name;
    private Address address;
}
