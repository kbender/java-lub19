package eu.sdacademy.warehouse;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WarehouseStore {

    private static volatile WarehouseStore instance;

    private Map<Long, Warehouse> warehouses = new HashMap<>();

    private AtomicInteger sequence = new AtomicInteger(0);

    public static WarehouseStore getInstance() {
        if (instance == null) {
            instance = new WarehouseStore();
        }

        return instance;
    }

    public void put(Warehouse warehouse) {
        warehouses.put(warehouse.getId(), warehouse);
    }

    public Warehouse get(Long id) {
        return warehouses.get(id);
    }

    public Warehouse getByName(String name) {
            return warehouses.values().stream()
                    .filter(warehouse -> warehouse.getName().equals(name))
                    .findFirst().orElse(null);
    }

    public Long getNewId() {
        return Long.valueOf(sequence.incrementAndGet());
    }
}
