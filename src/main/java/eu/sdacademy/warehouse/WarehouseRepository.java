package eu.sdacademy.warehouse;

import eu.sdacademy.address.Address;
import eu.sdacademy.address.AddressValidator;

import java.util.HashMap;
import java.util.Map;

import static eu.sdacademy.Utils.hasText;
import static eu.sdacademy.Utils.ERROR_REQUIRED_FIELD;
import static java.lang.String.format;

public class WarehouseRepository {

    WarehouseStore warehouseStore = WarehouseStore.getInstance();

    public Map<String, String> create(String name, String city,
                                      String street, String number,
                                      String postalCode, String country
    ) {
        Address address = Address.builder()
                .city(city)
                .street(street)
                .number(number)
                .country(country)
                .postalCode(postalCode)
                .build();

        Long id = warehouseStore.getNewId();

        Warehouse warehouse = new Warehouse(id, name, address);

        Map<String, String> errors = validate(warehouse);

        if (errors.isEmpty()) {
            warehouseStore.put(warehouse);
        }

        return errors;
    }

    public Map<String, String> validate(Warehouse warehouse) {

        Map<String, String> errors = new HashMap<>();

        if (!hasText(warehouse.getName())) {
            errors.put("name", format(ERROR_REQUIRED_FIELD, "Name"));
        }

        Address address = warehouse.getAddress();
        if (address != null) {
            errors.putAll(AddressValidator.validate(address));
        }

        return errors;
    }

    public Warehouse findByName(String name){
        return warehouseStore.getByName(name);
    }
}