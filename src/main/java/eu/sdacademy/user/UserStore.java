package eu.sdacademy.user;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserStore {

    private static volatile UserStore instance;

    Map<Long, User> users =  new HashMap<>();

    AtomicInteger sequence = new AtomicInteger(0);

    public Optional<User> findByUserName(String username){
        return users.values().stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst();
    }

    public boolean userExist(String username){
        return users.values().stream()
                .anyMatch(user -> user.getUsername().equals(username));
    }

    public void delete(User user){
        users.remove(user.getId());
    }

    public void put(User user){
        users.put(user.getId(), user);
    }

    public Long getNextId(){
        return Long.valueOf(sequence.incrementAndGet());
    }

    public static UserStore getInstance(){

        if (instance == null){
            synchronized (UserStore.class){
                instance = new UserStore();
            }
        }

        return instance;
    }

    public void update(User user) {
        delete(user);
        put(user);
    }
}
