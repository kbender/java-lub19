package eu.sdacademy.user;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Getter
@Setter
public class User {

    private Long id;
    private String firstName;
    private String lastName;

    private String username;
    private String passwordHash;

    private int type;

    private Date createdOn;
    private Date updatedOn;

}
