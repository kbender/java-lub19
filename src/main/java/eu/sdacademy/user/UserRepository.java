package eu.sdacademy.user;

import eu.sdacademy.Utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static eu.sdacademy.Utils.hasText;

public class UserRepository {

    private UserStore userStore = UserStore.getInstance();

    /**
     * Tworzy nowego użytkownika
     *
     * @return
     */
    public Map<String, String> create(String username,
                       String firstName,
                       String lastName,
                       String password){

        Long id = userStore.getNextId();

        User user = User.builder().id(id)
                .username(username)
                .firstName(firstName)
                .lastName(lastName)
                .passwordHash(Utils.generateHash(password))
                .createdOn(new Date())
                .updatedOn(new Date())
                .build();

        Map<String, String> errors = validate(user);

        if (errors.isEmpty()){
            userStore.put(user);
        }

        return errors;
    }

    /**
     * Aktualizacja danych użytkownika o podanym username.
     * @return
     */
    public Map<String, String> update(String username,
                                      String firstName,
                                      String lastName,
                                      String password){

        Map<String, String> errors = new HashMap<>();

        Optional<User> optional = userStore.findByUserName(username);

        if (optional.isPresent()) {
            User user = optional.get();

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPasswordHash(Utils.generateHash(password));
            user.setUpdatedOn(new Date());

            errors = validate(user);

            if (errors.isEmpty()){
                userStore.update(user);
            }
        } else {
            errors.put("user", "User not exists");
        }

        return errors;

    }

    public boolean delete(String username){
        Optional<User> optional = userStore.findByUserName(username);

        if (optional.isPresent()) {
            userStore.delete(optional.get());
            return true;
        } else {
            return false;
        }
    }

    public User findOneByUserName(String username){
        Optional<User> optional = userStore.findByUserName(username);

        return optional.orElseGet(null);
    }

    /**
     * Walidacja poprawności pól w obiekcie użytkownika.
     *
     * @param user
     * @return
     */
    private Map<String, String> validate(User user){

        Map<String, String> errors = new HashMap<>();

        if (user.getId() == null){
            errors.put("id",
                    "User id is required.");
        }

        String username = user.getUsername();

        if (!hasText(username)){
            errors.put("username",
                    "User username is required.");
        } else if (userStore.userExist(username)){
            errors.put("username",
                    "User with given username already exists.");
        }

        return errors;
    }
}
