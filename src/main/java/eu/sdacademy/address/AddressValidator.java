package eu.sdacademy.address;

import eu.sdacademy.Utils;

import java.util.HashMap;
import java.util.Map;

import static eu.sdacademy.Utils.hasText;
import static java.lang.String.format;

public class AddressValidator {

    public static Map<String, String> validate(Address address){

        Map<String, String> errors = new HashMap<>();

        if (!hasText(address.getCity())){
            errors.put("city",
                    format(Utils.ERROR_REQUIRED_FIELD,"City"));
        }

        if (!hasText(address.getCountry())){
            errors.put("country",
                    format(Utils.ERROR_REQUIRED_FIELD,"Country"));
        }

        if (!hasText(address.getNumber())){
            errors.put("number",
                    format(Utils.ERROR_REQUIRED_FIELD,"Number"));
        }

        if (!hasText(address.getPostalCode())){
            errors.put("postalCode",
                    format(Utils.ERROR_REQUIRED_FIELD,"Postal code"));
        }

        return errors;
    }
}
