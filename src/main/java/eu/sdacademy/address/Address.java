package eu.sdacademy.address;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Address {

    private String city;
    private String street;
    private String number;
    private String postalCode;
    private String country;
}
