package eu.sdacademy;

import org.apache.commons.codec.digest.DigestUtils;

public class Utils {

    public static final String ERROR_REQUIRED_FIELD = "'%s' is required.";

    public static String generateHash(String password) {
        return DigestUtils.md5Hex(password);
    }

    public static boolean hasText(String arg){

        return arg != null && arg.trim().length() != 0;
    }

}
