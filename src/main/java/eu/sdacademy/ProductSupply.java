package eu.sdacademy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class ProductSupply {

    private String productCode;
    private int count;
}
