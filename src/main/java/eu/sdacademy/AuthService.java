package eu.sdacademy;

import eu.sdacademy.user.User;
import eu.sdacademy.user.UserRepository;

public class AuthService {

    private UserRepository userRepository = new UserRepository();

    public boolean authenticate(String username, String password){

        User user = userRepository.findOneByUserName(username);

        String passwordHash = Utils.generateHash(password);

        return user.getPasswordHash().equals(passwordHash);
    }
}
