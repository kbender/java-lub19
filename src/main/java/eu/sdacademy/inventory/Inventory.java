package eu.sdacademy.inventory;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Inventory {

    private Long productId;
    private Long warehouseId;
    private int count;
}
