package eu.sdacademy.inventory;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InventoryStore {

    private static volatile InventoryStore instance;

    List<Inventory> inventoryData = new ArrayList<>();

    public static InventoryStore getInstance(){
        if (instance == null){
            synchronized (InventoryStore.class){
                instance = new InventoryStore();
            }
        }
        return instance;
    }

    public void add(Inventory inventory){
        inventoryData.add(inventory);
    }

    public void remove(Long productId, Long warehouseId){
        inventoryData = inventoryData.stream()
                .filter(inv ->
                        !(inv.getProductId()
                                .equals(productId)
                                && inv.getWarehouseId()
                                .equals(warehouseId)))
                .collect(Collectors.toList());
    }

    public void remove(Long productId) {
        inventoryData = inventoryData.stream()
                .filter(inv ->
                        !(inv.getProductId()
                                .equals(productId)))
                .collect(Collectors.toList());
    }

    public Inventory find(Long productId, Long warehouseId){

        return inventoryData.stream()
                .filter(inv ->
                        inv.getProductId()
                                .equals(productId)
                                && inv.getWarehouseId()
                                .equals(warehouseId))

                .findFirst().orElse(null);

    }
}
