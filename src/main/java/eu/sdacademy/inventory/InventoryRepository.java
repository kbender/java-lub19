package eu.sdacademy.inventory;

import eu.sdacademy.ProductSupply;
import eu.sdacademy.product.Product;
import eu.sdacademy.product.ProductRepository;
import eu.sdacademy.warehouse.Warehouse;

import java.util.ArrayList;
import java.util.List;

public class InventoryRepository {

    private InventoryStore store = InventoryStore.getInstance();

    private ProductRepository productRepository = new ProductRepository();


    private void supply(List<Inventory> inventories) {
        inventories.forEach(inventory -> store.add(inventory));
    }

    /**
     * W przypadku błędów zwraca listę zaopatrzenia
     * nie przyjętego na magazyn.
     *
     * @param warehouse
     * @param supply
     * @return
     */
    public List<ProductSupply> supply(Warehouse warehouse, List<ProductSupply> supply) {

        List<Inventory> inventories = new ArrayList<>();

        List<ProductSupply> supplyWithIncorrectCodes = new ArrayList<>();

        for (ProductSupply productSupply : supply) {

            String productCode = productSupply.getProductCode();

            Product product = productRepository.findByCode(productCode);

            if (product != null) {
                Inventory inv = new Inventory(product.getId(),
                        warehouse.getId(),
                        productSupply.getCount());

                inventories.add(inv);
            } else {

                supplyWithIncorrectCodes.add(productSupply);
            }
        }

        supply(inventories);

        return supplyWithIncorrectCodes;
    }

    public void deleteFromWarehouse(Warehouse warehouse, Product product){
        store.remove(product.getId(), warehouse.getId());
    }

    public void deleteFromWarehouse(Product product){
        store.remove(product.getId());
    }

    public boolean checkIsAvailable(Warehouse warehouse, Product product, int count) {

        Inventory inventory = store.find(product.getId(), warehouse.getId());

        if (inventory != null){
            return inventory.getCount() > count;
        } else {
            return false;
        }
    }
}
