package eu.sdacademy.inventory;

import eu.sdacademy.ProductSupply;
import eu.sdacademy.warehouse.Warehouse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InventoryManager {

    private InventoryRepository inventoryRepository = new InventoryRepository();

    public List<ProductSupply> supplyFromFile(Warehouse warehouse, String fileName) throws IOException {


        BufferedReader csvReader = new BufferedReader(new FileReader(fileName));

        String row;
        List<ProductSupply> supply = new ArrayList<>();

        while ((row = csvReader.readLine()) != null) {


            String[] data = row.split(",");


            // TODO -
            // Product code
            String productCode = data[0].replace("\"", "").trim();
            // Count
            int count = Integer.parseInt(data[1].trim());

            supply.add(new ProductSupply(productCode, count));
        }

        csvReader.close();

        return inventoryRepository.supply(warehouse, supply);
    }
}
