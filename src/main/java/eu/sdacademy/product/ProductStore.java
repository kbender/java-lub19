package eu.sdacademy.product;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductStore {

    private static volatile ProductStore instance;

    private Map<Long, Product> products = new HashMap<>();

    private AtomicInteger sequence = new AtomicInteger(0);

    public static ProductStore getInstance(){

        if (instance == null){

            synchronized (ProductStore.class){

                instance =  new ProductStore();
            }
        }

        return instance;
    }

    public void put(Product product){
        products.put(product.getId(), product);
    }

    public Product get(Long id){
        return products.get(id);
    }

    public Optional<Product> get(String name){

        return products.values().stream()
                .filter(product -> product.getName().equals(name))
                .findFirst();
    }

    public Optional<Product> getByCode(String code){

        return products.values().stream()
                .filter(product -> product.getCode().equals(code))
                .findFirst();
    }

    public Long getNewId() {
        return Long.valueOf(sequence.incrementAndGet());
    }

    public void update(Product product) {
        if (products.containsKey(product.getId())) {
            products.put(product.getId(), product);
        }
    }
}
