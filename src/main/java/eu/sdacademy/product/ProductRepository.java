package eu.sdacademy.product;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static eu.sdacademy.Utils.hasText;
import static eu.sdacademy.Utils.ERROR_REQUIRED_FIELD;
import static java.lang.String.format;

public class ProductRepository {


    private ProductStore store = ProductStore.getInstance();


    public Map<String, String> create( String name, String code,
                        Date expirationDate, ProductUnit unit){

        Long id = store.getNewId();

        Product product = new Product(id,name, code, expirationDate, unit);

        Map<String, String> errors =  validate(product);

        if (errors.isEmpty()){
            store.put(product);
        }

        return errors;
    }

    public Map<String, String> update(String code,String name,
                                      Date expirationDate, ProductUnit unit){

        Map<String, String> errors =  new HashMap<>();

        Optional<Product> optionalProduct = store.getByCode(code);

        if (optionalProduct.isPresent()){
            Product product = optionalProduct.get();

            product.setName(name);
            product.setExpirationDate(expirationDate);
            product.setUnit(unit);

            errors.putAll(validate(product));

            if (errors.isEmpty()){
                store.update(product);
            }

        } else {
            errors.put("product", "Product with given code not exists.");
        }

        return errors;
    }


    private Map<String, String> validate(Product product){

        Map<String, String> errors =  new HashMap<>();

        if (!hasText(product.getName())){
            errors.put("name", format(ERROR_REQUIRED_FIELD, "Name"));
        }

        if (!hasText(product.getCode())){
            errors.put("code", format(ERROR_REQUIRED_FIELD, "Code"));
        }

        // ...

        return errors;
    }

    public Product findByCode(String productCode) {
        return store.getByCode(productCode).orElse(null);
    }
}
