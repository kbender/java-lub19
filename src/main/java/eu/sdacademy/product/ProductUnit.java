package eu.sdacademy.product;

public enum ProductUnit {

    /**
     * Waga
     */
    WEIGHT,
    /**
     * Pojemność
     */
    CAPACITY,
    /**
     * Długość
     */
    LENGTH,

    PIECE,

}
