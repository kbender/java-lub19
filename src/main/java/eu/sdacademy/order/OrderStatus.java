package eu.sdacademy.order;

public enum OrderStatus {

    /**
     * Nowe, nie przyjęte do realizacji
     */
    NEW,
    /**
     * W trakcie realizacji
     */
    IN_PROGRESS,
    /**
     * Zrealizowane
     */
    COMPLETED

}
