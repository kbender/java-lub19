package eu.sdacademy.order;

import eu.sdacademy.product.ProductUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class OrderItem {

    private Long productId;
    private int count;
    private ProductUnit unit;
    private BigDecimal price;
    private double tax;

}
