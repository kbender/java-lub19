package eu.sdacademy.order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Order {

    private Long id;
    private Date createdOn;
    private Date updatedOn;

    private Customer customer;

    private List<OrderItem> items;

    private OrderStatus status;
    private Payment payment;

    /**
     * Cena całości z podatkami.
     *
     * net
     * gross
     *
     * @return
     */
    public BigDecimal getPriceNet(){

        BigDecimal sum = new BigDecimal(0);
        for (OrderItem item: items) {
            sum.add(item.getPrice());
        }

        return sum;
    }

    public BigDecimal getPriceGross(){

        BigDecimal sum = new BigDecimal(0);
        for (OrderItem item: items) {
            sum.add(BigDecimal.valueOf(item.getPrice().doubleValue() * (1 + item.getTax())) );
        }

        return sum;
    }
}
