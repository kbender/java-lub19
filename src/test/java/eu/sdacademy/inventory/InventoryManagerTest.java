package eu.sdacademy.inventory;

import eu.sdacademy.ProductSupply;
import eu.sdacademy.product.ProductRepository;
import eu.sdacademy.product.ProductUnit;
import eu.sdacademy.warehouse.Warehouse;
import eu.sdacademy.warehouse.WarehouseRepository;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class InventoryManagerTest {

    @Test
    public void supplyFromFile() {

        // begin

        InventoryManager inventoryManager = new InventoryManager();
        WarehouseRepository warehouseRepository = new WarehouseRepository();

        Map<String, String> errors = warehouseRepository.create("LUB.101",
                "Lublin",
                "Nałęczowska",
                "16",
                "20-830", "Polska");
        assertTrue(errors.isEmpty());

        Warehouse warehouse = warehouseRepository.findByName("LUB.101");

        // Create products

        ProductRepository productRepository = new ProductRepository();

        errors = productRepository.create("Produkt.01",
                "Produkt.01", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());

        errors = productRepository.create("Produkt.02",
                "Produkt.02", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());

        errors = productRepository.create("Produkt.03",
                "Produkt.03", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());

        // when

        try {
            List<ProductSupply> withErrors = inventoryManager
                    .supplyFromFile(warehouse,
                            "src/main/resources/supply.csv");

            withErrors.forEach(productSupply -> {
                System.out.println(productSupply);
            });

            assertEquals(2, withErrors.size());

            assertTrue(withErrors.stream()
                    .filter(error -> error.getProductCode()
                            .equals("P234"))
                    .findFirst().isPresent()
            );

            assertTrue(withErrors.stream()
                    .filter(error -> error.getProductCode()
                            .equals("Produkt.01"))
                    .findFirst().isPresent()
            );

        } catch (IOException e) {
            e.printStackTrace();
        }

        // then
    }
}