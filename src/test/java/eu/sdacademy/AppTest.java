package eu.sdacademy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import eu.sdacademy.inventory.InventoryManager;
import eu.sdacademy.inventory.InventoryRepository;
import eu.sdacademy.product.Product;
import eu.sdacademy.product.ProductRepository;
import eu.sdacademy.product.ProductUnit;
import eu.sdacademy.user.UserRepository;
import eu.sdacademy.warehouse.Warehouse;
import eu.sdacademy.warehouse.WarehouseRepository;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AppTest
{
    private static UserRepository userRepository = new UserRepository();
    private static WarehouseRepository warehouseRepository = new WarehouseRepository();
    private static ProductRepository productRepository = new ProductRepository();
    private static AuthService authService = new AuthService();
    private static InventoryRepository inventoryRepository = new InventoryRepository();

    @BeforeClass
    public static void prepareSampleData(){

        // Create users

        Map<String, String> errors
                = userRepository.create("tester.01",
                "T", "01", "pass.01");
        assertTrue(errors.isEmpty());

        errors = userRepository.create("tester.02",
                "T", "02", "pass.02");
        assertTrue(errors.isEmpty());

        errors = userRepository.create("tester.03",
                "T", "03", "pass.03");
        assertTrue(errors.isEmpty());

        // Create warehouses

        errors = warehouseRepository.create("LUB.001",
                "Lublin",
                "Nałęczowska",
                "16",
                "20-830", "Polska");
        assertTrue(errors.isEmpty());
        errors = warehouseRepository.create("LUB.002",
                "Lublin",
                "Nałęczowska",
                "17",
                "20-830", "Polska");
        assertTrue(errors.isEmpty());


        // Create products
        errors = productRepository.create("Produkt.01",
                "Produkt.01", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());

        errors = productRepository.create("Produkt.02",
                "Produkt.02", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());

        errors = productRepository.create("Produkt.03",
                "Produkt.03", new Date(), ProductUnit.WEIGHT);
        assertTrue(errors.isEmpty());


        // Read warehouse inventory from file

        InventoryManager inventoryManager = new InventoryManager();

        Warehouse warehouse = warehouseRepository.findByName("LUB.001");

        try {
            List<ProductSupply> withErrors = inventoryManager
                    .supplyFromFile(warehouse,
                            "src/main/resources/supply.csv");

            withErrors.forEach(error -> {
                System.out.println(error);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMyApplicationLogin()
    {
        // when
        boolean authenticate = authService.authenticate("tester.01", "pass.01");
        // then
        assertTrue(authenticate);

        // when
        Warehouse warehouse = warehouseRepository.findByName("LUB.001");
        Product product = productRepository.findByCode("Produkt.01");
        inventoryRepository.deleteFromWarehouse(warehouse, product);

        // then
        boolean isAvailable = inventoryRepository.checkIsAvailable(warehouse, product, 1);

        assertFalse(isAvailable);
    }
}
