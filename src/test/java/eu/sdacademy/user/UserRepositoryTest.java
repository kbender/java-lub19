package eu.sdacademy.user;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class UserRepositoryTest {

    @BeforeClass
    public static void prepare(){
        UserRepository userRepository = new UserRepository();
        userRepository.delete("krzysiek.b");
    }

    @Test
    public void testCreateWithExistedUsername() {
        // begin
        UserRepository userRepository = new UserRepository();

        // when
        Map<String, String> errors = userRepository.create("krzysiek.b",
                "K", "N", "xx124qq");
        // then
        assertTrue(errors.isEmpty());

        // when
        Map<String, String> expectedErrors = userRepository.create("krzysiek.b",
                "K", "N", "xx124qq");

        // then
        assertFalse(expectedErrors.isEmpty());

        String error = expectedErrors.get("username");

        assertEquals("User with given username already exists.",
                error);
    }

    @Test
    public void testCreateWithNullUsername(){
        // begin
        UserRepository userRepository = new UserRepository();

        // when
        Map<String, String> expectedErrors = userRepository.create(null,
                "K", "N", "xx124qq");

        // then
        assertFalse(expectedErrors.isEmpty());

        String error = expectedErrors.get("username");

        assertEquals("User username is required.", error);

    }


    @Test
    public void testCreateWithEmptyUsername(){
        // begin
        UserRepository userRepository = new UserRepository();

        // when
        Map<String, String> expectedErrors = userRepository.create("",
                "K", "N", "xx124qq");

        // then
        assertFalse(expectedErrors.isEmpty());

        String error = expectedErrors.get("username");

        assertEquals("User username is required.", error);
    }
}