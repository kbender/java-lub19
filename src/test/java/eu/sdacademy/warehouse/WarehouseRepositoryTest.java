package eu.sdacademy.warehouse;

import eu.sdacademy.Utils;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class WarehouseRepositoryTest {

    @Test
    public void createWarehouse() {

        // begin
        WarehouseRepository repository = new WarehouseRepository();

        // when
        Map<String, String> errors = repository.create("LUB001",
                "Lublin",
                "Nałęczowska",
                "16",
                "20-830", "Polska");

        // then
        assertTrue(errors.isEmpty());
    }

    @Test
    public void createWarehouseWithEmptyName(){
        // begin
        WarehouseRepository repository = new WarehouseRepository();

        // when
        Map<String, String> errors = repository.create("",
                "Lublin",
                "Nałęczowska",
                "16",
                "20-830", "Polska");

        // then
        assertFalse(errors.isEmpty());

        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Name"),
                errors.get("name"));
    }

    @Test
    public void createWarehouseWithNullName(){
        // begin
        WarehouseRepository repository = new WarehouseRepository();

        // when
        Map<String, String> errors = repository.create(null,
                "Lublin",
                "Nałęczowska",
                "16",
                "20-830", "Polska");

        // then
        assertFalse(errors.isEmpty());
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Name")
                , errors.get("name"));
    }

    @Test
    public void createWarehouseWithNullValues(){
        // begin
        WarehouseRepository repository = new WarehouseRepository();

        // when
        Map<String, String> errors = repository.create(null,
                "",
                "",
                "",
                "", "");

        // then
        assertFalse(errors.isEmpty());
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Name")
                , errors.get("name"));
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "City")
                , errors.get("city"));
        assertNull(errors.get("street"));
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Number")
                , errors.get("number"));
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Postal code")
                , errors.get("postalCode"));
        assertEquals(String.format(Utils.ERROR_REQUIRED_FIELD, "Country")
                , errors.get("country"));
    }

}