package eu.sdacademy;

import eu.sdacademy.user.UserRepository;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class AuthServiceTest {

    @Test
    public void authenticateWithCorrectPassword() {

        // begin

        AuthService authService = new AuthService();
        UserRepository userRepository = new UserRepository();

        // when
        Map<String, String> errors = userRepository.create(
                "krzysiek.b",
                "K", "N",
                "xx124qq");

        boolean authenticate = authService.authenticate(
                "krzysiek.b", "xx124qq");

        // then
        assertTrue(errors.isEmpty());
        assertTrue(authenticate);
    }


    @Test
    public void authenticateWithIncorrectPassword() {

        // begin
        AuthService authService = new AuthService();
        UserRepository userRepository = new UserRepository();

        // when
        Map<String, String> errors = userRepository.create(
                "krzysiek.b.01",
                "K", "N",
                "QWERTTY");

        boolean authenticate = authService.authenticate(
                "krzysiek.b.01", "xx124qq");

        // then
        assertTrue(errors.isEmpty());
        assertFalse(authenticate);
    }

}